#pragma once

#include "chrono_vehicle/ChVehicleModelData.h"
#include "chrono_vehicle/driver/ChIrrGuiDriver.h"
#include "chrono_vehicle/driver/ChPathFollowerDriver.h"
#include "chrono_vehicle/utils/ChVehiclePath.h"


// =============================================================================
// Select Path Follower, uncomment to select the pure PID steering controller
#define USE_PID 1
// The extended steering controller only works inside the path limits
// #define USE_XT 1
// Stanley controller
// #define USE_ST 1
// =============================================================================

using namespace chrono;
using namespace chrono::geometry;
using namespace chrono::vehicle;
using namespace chrono::vehicle::hmmwv;

namespace MY_DRIVER{
    
    // Custom Irrlicht event receiver for selecting current driver model.
    class ChDriverSelector : public irr::IEventReceiver {
    public:
#ifdef USE_PID
        ChDriverSelector(const ChVehicle& vehicle,
         ChPathFollowerDriver* driver_follower, 
         ChIrrGuiDriver* driver_gui):m_vehicle(vehicle), 
         m_driver_follower(driver_follower), 
         m_driver_gui(driver_gui), 
         m_driver(driver_gui), 
         m_using_gui(true) 
        {
            set_driver_gui();
        }
#endif
#ifdef USE_XT
        ChDriverSelector(const ChVehicle& vehicle, ChPathFollowerDriverXT* driver_follower, ChIrrGuiDriver* driver_gui)
        : m_vehicle(vehicle),
        m_driver_follower(driver_follower),
        m_driver_gui(driver_gui),
        m_driver(driver_gui),
        m_using_gui(true) {
            set_driver_gui();
        }
#endif
#ifdef USE_ST
        ChDriverSelector(const ChVehicle& vehicle, ChPathFollowerDriverStanley* driver_follower, ChIrrGuiDriver* driver_gui)
            : m_vehicle(vehicle),
            m_driver_follower(driver_follower),
            m_driver_gui(driver_gui),
            m_driver(driver_gui),
            m_using_gui(true) {
                set_driver_gui();
            }
#endif
        ChDriver* GetDriver() { return m_driver; }
        bool UsingGUI() const { return m_using_gui; }

        virtual bool OnEvent(const irr::SEvent& event) {
            // Only interpret keyboard inputs.
            if (event.EventType != irr::EET_KEY_INPUT_EVENT)
                return false;

            // Disregard key pressed
            if (event.KeyInput.PressedDown)
                return false;

            switch (event.KeyInput.Key) {
                case irr::KEY_COMMA:
                    if (m_using_gui) {
                        m_driver = m_driver_follower;
                        m_using_gui = false;
                    }
                    return true;
                case irr::KEY_PERIOD:
                    if (!m_using_gui) {
                        m_driver_gui->SetThrottle(m_driver_follower->GetThrottle());
                        m_driver_gui->SetSteering(m_driver_follower->GetSteering());
                        m_driver_gui->SetBraking(m_driver_follower->GetBraking());
                        m_driver = m_driver_gui;
                        m_using_gui = true;
                    }
                    return true;
                case irr::KEY_HOME:
                    if (!m_using_gui && !m_driver_follower->GetSteeringController().IsDataCollectionEnabled()) {
                        std::cout << "Data collection started at t = " << m_vehicle.GetChTime() << std::endl;
                        m_driver_follower->GetSteeringController().StartDataCollection();
                    }
                    return true;
                case irr::KEY_END:
                    if (!m_using_gui && m_driver_follower->GetSteeringController().IsDataCollectionEnabled()) {
                        std::cout << "Data collection stopped at t = " << m_vehicle.GetChTime() << std::endl;
                        m_driver_follower->GetSteeringController().StopDataCollection();
                    }
                    return true;
                case irr::KEY_INSERT:
                    if (!m_using_gui && m_driver_follower->GetSteeringController().IsDataAvailable()) {
                        char filename[100];
                        sprintf(filename, "controller_%.2f.out", m_vehicle.GetChTime());
                        std::cout << "Data written to file " << filename << std::endl;
                        m_driver_follower->GetSteeringController().WriteOutputFile(std::string(filename));
                    }
                    return true;
                default:
                    break;
            }

            return false;
        }

    private:
        void set_driver_gui(){

            // Set the time response for steering and throttle keyboard inputs.
            // 50Hz -> 0.02 1/s
            double steering_step = 0.02;  // time to go from 0 to +1 (or from 0 to -1)
            double throttle_step = 0.02;  // time to go from 0 to +1
            double braking_step = 0.06;   // time to go from 0 to +1
            m_driver_gui->SetSteeringDelta(steering_step);
            m_driver_gui->SetThrottleDelta(throttle_step);
            m_driver_gui->SetBrakingDelta(braking_step);
            m_driver_gui->Initialize();
        }
        bool m_using_gui;
        const ChVehicle& m_vehicle;
#ifdef USE_PID
        ChPathFollowerDriver* m_driver_follower;
#endif
#ifdef USE_XT
        ChPathFollowerDriverXT* m_driver_follower;
#endif
#ifdef USE_ST
        ChPathFollowerDriverStanley* m_driver_follower;
#endif
        ChIrrGuiDriver* m_driver_gui;
        ChDriver* m_driver;
    };

}