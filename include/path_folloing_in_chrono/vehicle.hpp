#pragma once

#include "chrono_models/vehicle/hmmwv/HMMWV.h"


namespace MY_VEHICLE
{
    using namespace chrono;
    using namespace chrono::vehicle;

    /// @brief Generate a HMMWV_Full with the given calibration
    /// @param initLoc          : Init location
    /// @param initRot          : Init orientation (quaternion)
    /// @param tire_step_size   : 
    /// @param contact_method   : Contact method: constraint-based vs penalty-based (NSC, SMC)
    /// @param vis_type         : Visualization type for vehicle parts (PRIMITIVES, MESH, or NONE)
    /// @param tire_model       : Type of tire model (RIGID, RIGID_MESH, TMEASY, PACEJKA, LUGRE, FIALA, PAC89, PAC02)
    /// @param drive_type       : Drive type (FWD, RWD, or AWD)

    /// @return With a calibrated vehicle
    chrono::vehicle::hmmwv::HMMWV_Full generate_new_vehicle(ChVector<> initLoc, ChQuaternion<> initRot, double tire_step_size, ChContactMethod contact_method, VisualizationType vis_type = VisualizationType::NONE, TireModelType tire_model = TireModelType::TMEASY, DrivelineTypeWV drive_type = DrivelineTypeWV::AWD)
    {
        chrono::vehicle::hmmwv::HMMWV_Full vehicle;

        // Parameters
        vehicle.SetInitPosition(ChCoordsys<>(initLoc, initRot));
        vehicle.SetContactMethod(contact_method);
        vehicle.SetDriveType(drive_type);
        vehicle.SetTireType(tire_model);
        vehicle.SetTireStepSize(tire_step_size);

        vehicle.SetChassisCollisionType(CollisionType::MESH);
        vehicle.SetChassisFixed(false);
        vehicle.SetPowertrainType(PowertrainModelType::SHAFTS);
        vehicle.SetSteeringType(SteeringTypeWV::PITMAN_ARM);
        
        // Initialization
        vehicle.Initialize();

        // Visualization properties
        vehicle.SetChassisVisualizationType(vis_type);
        vehicle.SetSuspensionVisualizationType(vis_type);
        vehicle.SetSteeringVisualizationType(vis_type);
        vehicle.SetWheelVisualizationType(vis_type);
        vehicle.SetTireVisualizationType(chrono::vehicle::VisualizationType::MESH);
        GetLog() << "Vehicle initialised.\n";
        return vehicle;
    }

    /// @brief Generate a HMMWV_Reduced with the given calibration
    /// @param initLoc          : Init location
    /// @param initRot          : Init orientation (quaternion)
    /// @param tire_step_size   : 
    /// @param contact_method   : Contact method: constraint-based vs penalty-based (NSC, SMC)
    /// @param vis_type         : Visualization type for vehicle parts (PRIMITIVES, MESH, or NONE)
    /// @param tire_model       : Type of tire model (RIGID, RIGID_MESH, TMEASY, PACEJKA, LUGRE, FIALA, PAC89, PAC02)
    /// @param drive_type       : Drive type (FWD, RWD, or AWD)

    /// @return With a calibrated vehicle
    chrono::vehicle::hmmwv::HMMWV_Reduced generate_new_rd_vehicle(ChVector<> initLoc, ChQuaternion<> initRot, double tire_step_size, ChContactMethod contact_method, VisualizationType vis_type = VisualizationType::NONE, TireModelType tire_model = TireModelType::TMEASY, DrivelineTypeWV drive_type = DrivelineTypeWV::AWD)
    {
        chrono::vehicle::hmmwv::HMMWV_Reduced vehicle;

        // Parameters
        vehicle.SetInitPosition(ChCoordsys<>(initLoc, initRot));
        vehicle.SetContactMethod(contact_method);
        vehicle.SetDriveType(drive_type);
        vehicle.SetTireType(tire_model);
        vehicle.SetTireStepSize(tire_step_size);

        vehicle.SetChassisCollisionType(CollisionType::MESH);
        vehicle.SetChassisFixed(false);
        vehicle.SetPowertrainType(PowertrainModelType::SHAFTS);

        
        // Initialization
        vehicle.Initialize();

        // Visualization properties
        vehicle.SetChassisVisualizationType(vis_type);
        vehicle.SetSuspensionVisualizationType(vis_type);
        vehicle.SetSteeringVisualizationType(vis_type);
        vehicle.SetWheelVisualizationType(vis_type);
        vehicle.SetTireVisualizationType(chrono::vehicle::VisualizationType::MESH);
        GetLog() << "Vehicle initialised.\n";
        return vehicle;
    }
}