#pragma once

#include "chrono_vehicle/terrain/RigidTerrain.h"
#include "chrono_vehicle/terrain/SCMDeformableTerrain.h"

namespace MY_TERRAIN{
    // Custom callback for setting location-dependent soil properties.
    // Note that the (x,y) location is given in the terrain's reference plane.
    // Here, the vehicle moves in the terrain's negative y direction!
    class MySoilParams : public vehicle::SCMDeformableTerrain::SoilParametersCallback {
    public:
        virtual void Set(double x, double y) override {
            if (y > -5) {
                m_Bekker_Kphi = 0.2e6;
                m_Bekker_Kc = 0;
                m_Bekker_n = 1.1;
                m_Mohr_cohesion = 0;
                m_Mohr_friction = 30;
                m_Janosi_shear = 0.01;
                m_elastic_K = 4e7;
                m_damping_R = 3e4;
            } else {
                m_Bekker_Kphi = 5301e3;
                m_Bekker_Kc = 102e3;
                m_Bekker_n = 0.793;
                m_Mohr_cohesion = 1.3e3;
                m_Mohr_friction = 31.1;
                m_Janosi_shear = 1.2e-2;
                m_elastic_K = 4e8;
                m_damping_R = 3e4;
            }
        }
    };

    SCMDeformableTerrain generateDeformableTerrain(ChSystem* system, bool grid_viz = false){

        // Rigid terrain
        RigidTerrain::PatchType terrain_model = RigidTerrain::PatchType::MESH;
        double terrainHeight = 0;      // terrain height (FLAT terrain only)
        double terrainLength = 10000.0;  // size in X direction
        double terrainWidth = 10000.0;   // size in Y direction

        SCMDeformableTerrain terrain(system);
            terrain.SetSoilParameters(2e6,   // Bekker Kphi
                                0,     // Bekker Kc
                                1.1,   // Bekker n exponent
                                0,     // Mohr cohesive limit (Pa)
                                30,    // Mohr friction limit (degrees)
                                0.01,  // Janosi shear coefficient (m)
                                2e8,   // Elastic stiffness (Pa/m), before plastic yield
                                3e4    // Damping (Pa s/m), proportional to negative vertical speed (optional)
        );

        // auto my_params = chrono_types::make_shared<MY_TERRAIN::MySoilParams>();
        // terrain.RegisterSoilParametersCallback(my_params);

        terrain.EnableBulldozing(true);  // inflate soil at the border of the rut
        terrain.SetBulldozingParameters(
            55,   // angle of friction for erosion of displaced material at the border of the rut
            1,    // displaced material vs downward pressed material.
            5,    // number of erosion refinements per timestep
            6);  // number of concentric vertex selections subject to erosion

        terrain.SetPlotType(vehicle::SCMDeformableTerrain::PLOT_SINKAGE, 0, 0.15);

        // terrain.Initialize(vehicle::GetDataFile("terrain/height_maps/test64.bmp"), 200.0, 200.0, 0, 4.0, 0.05);
        terrain.Initialize("E:/doba/path_following_in_chrono/map/test_200x200x11_2/test.bmp", 200.0, 200.0, 0, 11.2, 0.05);
        terrain.SetTexture("E:/doba/path_following_in_chrono/map/test_200x200x11_2/test_sat.jpg");
        // terrain.Initialize("E:/doba/path_following_in_chrono/map/test_slope0_8/test.bmp", 200.0, 200.0, 0, 5.0, 0.05);
        // terrain.SetTexture(vehicle::GetDataFile("terrain/textures/grass.jpg"), 10, 10);
        terrain.GetMesh()->SetWireframe(grid_viz);


        return terrain;
    }

    RigidTerrain generateRigidTerrain(ChSystem* system, ChContactMethod contact_method){

        // Rigid terrain
        RigidTerrain::PatchType terrain_model = RigidTerrain::PatchType::HEIGHT_MAP;
        double terrainHeight = 11.2;      // terrain height
        double terrainLength = 200.0;  // size in X direction
        double terrainWidth = 200.0;   // size in Y direction

        // ChCoordsys<double> new_pos = ChCoordsys<double>(ChVector<double>(0,0,0), ChQuaternion<double>(0, 0, 0, 1));

        RigidTerrain terrain(system);

        MaterialInfo minfo;
        minfo.mu = 0.9f;
        minfo.cr = 0.01f;
        minfo.Y = 2e7f;
        auto patch_mat = minfo.CreateMaterial(contact_method);

        std::shared_ptr<RigidTerrain::Patch> patch;
        switch (terrain_model) {
            case RigidTerrain::PatchType::BOX:
                patch = terrain.AddPatch(patch_mat, ChVector<>(0, 0, 0), ChVector<>(0, 0, 1), 1000, 1000);
                patch->SetTexture(vehicle::GetDataFile("terrain/textures/tile4.jpg"));
                break;
            case RigidTerrain::PatchType::HEIGHT_MAP:
                GetLog() << "Patch... \n";
                // patch = terrain.AddPatch(patch_mat, CSYSNORM, vehicle::GetDataFile("terrain/height_maps/test64.bmp"),
                patch = terrain.AddPatch(patch_mat, CSYSNORM, "E:/doba/path_following_in_chrono/map/test_200x200x11_2/test.bmp",
                                        "test64", terrainLength, terrainWidth, 0, terrainHeight);
                // patch->SetTexture(vehicle::GetDataFile("terrain/textures/grass.jpg"), 1, 1);
                patch->SetTexture("E:/doba/path_following_in_chrono/map/test_200x200x11_2/test_sat.jpg");
                break;
            case RigidTerrain::PatchType::MESH:
                // patch = terrain.AddPatch(patch_mat, CSYSNORM, vehicle::GetDataFile("terrain/height_maps/test64.bmp"), "test_mesh");
                patch->SetTexture(vehicle::GetDataFile("terrain/textures/grass.jpg"), 1, 1);
                break;
        }

        terrain.Initialize();
        return terrain;
    }


}