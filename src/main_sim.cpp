#include <chrono>
#include <string>
#include <format>

#include "chrono/core/ChStream.h"
#include "chrono/core/ChRealtimeStep.h"
#include "chrono/utils/ChUtilsInputOutput.h"
#include "chrono/utils/ChFilters.h"

#include "chrono_vehicle/wheeled_vehicle/utils/ChWheeledVehicleIrrApp.h"

#include "chrono_thirdparty/filesystem/path.h"

#include "path_folloing_in_chrono/vehicle.hpp"
#include "path_folloing_in_chrono/driver.hpp"
#include "path_folloing_in_chrono/terrain.hpp"


using namespace chrono;
using namespace chrono::irrlicht;
using namespace chrono::vehicle;
using namespace chrono::vehicle::hmmwv;

// =============================================================================
// SetChronoDataPath(CHRONO_DATA_DIR);

// Initial vehicle location and orientation
ChVector<> initLoc(-93, -93, 12);
// ChVector<> initLoc(-125, -125, 1.0);

// ChQuaternion<> initRot(1, 0, 0, 0);   // 180 deg
ChQuaternion<> initRot(0.9238795, 0, 0, 0.3826834);   // 135 deg
// ChQuaternion<> initRot(0.7071068, 0, 0, 0.7071068);   // 90 deg
// ChQuaternion<> initRot(0.3826834, 0, 0, 0.9238795);    // 45 deg
// ChQuaternion<> initRot(0, 0, 0, 1);   // 0 deg
// ChQuaternion<> initRot(-0.3826834, 0, 0, 0.9238795);    // -45 deg
// ChQuaternion<> initRot(-0.7071068, 0, 0, 0.7071068);    // -90 deg
// ChQuaternion<> initRot(-0.9238795, 0, 0, 0.3826834);    // -135 deg

// Contact method
ChContactMethod contact_method = ChContactMethod::SMC;
bool contact_vis = false;


// Input file names for the path-follower driver model
////std::string path_file("paths/straight.txt");
////std::string path_file("paths/curve.txt");
// std::string path_file("paths/NATO_double_lane_change.txt");  

// std::string path_file("paths/ISO_double_lane_change3.txt");
std::string path_file("paths/sample.txt");
// Desired vehicle speed (m/s)
double target_speed = 3;

// Point on chassis tracked by the camera
ChVector<> trackPoint(0.0, 0.0, 1.75);

// Simulation step sizes
double step_size = 3e-3;
double tire_step_size = 1e-3;

// Time interval between two render frames
double render_step_size = 1.0 / 20;  // FPS = 30

// Simulation end time
double t_end = 1000;

// Debug logging
bool debug_output = true;
double debug_step_size = 1.0 / 5;  // FPS = 1

// POV-Ray output
bool povray_output = false;

enum DriverMode { DEFAULT, RECORD, PLAYBACK };
DriverMode driver_mode = DriverMode::RECORD;

// Output directories
std::string out_dir = "E:/doba/path_following_in_chrono/log";
std::string pov_dir = out_dir + "/POVRAY";

std::string NowToString()
{
  std::chrono::system_clock::time_point p = std::chrono::system_clock::now();
  time_t t = std::chrono::system_clock::to_time_t(p);
  char str[100];
  auto curr_tm = localtime(&t);
  strftime(str, 50, "%Y%m%d_%H%M%S/", curr_tm);
  return str;
};

// =============================================================================

int main(int argc, char* argv[]) {
    GetLog() << "Copyright (c) 2017 projectchrono.org\nChrono version: " << CHRONO_VERSION << "\n\n";


    // Set path to Chrono data directory
    SetChronoDataPath(CHRONO_DATA_DIR);

    // ---------------------------------------------------------------------------------------------------------------
    // Create the HMMWV vehicle, set parameters, and initialize
    GetLog() << "Load vehicle... \n";
    // HMMWV_Full my_hmmwv = MY_VEHICLE::generate_new_vehicle(initLoc, initRot, tire_step_size, contact_method, VisualizationType::MESH, TireModelType::RIGID);
    HMMWV_Reduced my_hmmwv = MY_VEHICLE::generate_new_rd_vehicle(initLoc, initRot, tire_step_size, contact_method, VisualizationType::PRIMITIVES, TireModelType::RIGID);
    
    // ---------------------------------------------------------------------------------------------------------------
    // Create the vehicle Irrlicht interface
    ChWheeledVehicleIrrApp app(&my_hmmwv.GetVehicle(), L"HMMWV Demo");
    GetLog() << "Vehicle initialised.\n";

    // ---------------------------------------------------------------------------------------------------------------
    GetLog() << "Set solver... \n";
    ChSystem* system = my_hmmwv.GetSystem();
    // Set number of used threads
    system->SetNumThreads(std::min(8, ChOMP::GetNumProcs()));
    
    // Solver settings.
    system->SetSolverMaxIterations(50);

    GetLog() << "Solver initialised. \n";


    // ---------------------------------------------------------------------------------------------------------------
    // Create the terrain
    GetLog() << "Load terrain... \n";

    // auto terrain = MY_TERRAIN::generateRigidTerrain(system, contact_method);
    auto terrain = MY_TERRAIN::generateDeformableTerrain(system, false);

    for (auto& axle : my_hmmwv.GetVehicle().GetAxles()) {
       terrain.AddMovingPatch(axle->m_wheels[0]->GetSpindle(), ChVector<>(0, 0, 0), ChVector<>(1, 0.5, 1));
       terrain.AddMovingPatch(axle->m_wheels[1]->GetSpindle(), ChVector<>(0, 0, 0), ChVector<>(1, 0.5, 1));
    }

  
    GetLog() << "Terrain initialised.\n";




    // -----------------
    // Initialize output
    // -----------------
 //TODO: LOGGING
    if (!filesystem::create_directory(filesystem::path(out_dir))) {
        std::cout << "Error creating directory " << out_dir << std::endl;
        return 1;
    }
    // if (povray_output) {
    //     if (!filesystem::create_directory(filesystem::path(pov_dir))) {
    //         std::cout << "Error creating directory " << pov_dir << std::endl;
    //         return 1;
    //     }
    //     terrain.ExportMeshPovray(out_dir);
    // }

    // Initialize output file for driver inputs
    std::string driver_file = out_dir + "/mes_" + NowToString() +".txt";
    utils::CSV_writer driver_csv(" ");

    // Set up vehicle output
    // my_hmmwv.GetVehicle().SetChassisOutput(true);
    // my_hmmwv.GetVehicle().SetSuspensionOutput(0, true);
    // my_hmmwv.GetVehicle().SetSteeringOutput(0, true);
    // my_hmmwv.GetVehicle().SetOutput(ChVehicleOutput::JSON, out_dir, "output", 0.1);

    // Generate JSON information with available output channels
    // my_hmmwv.GetVehicle().ExportComponentList(out_dir + "/component_list.json");




    // ---------------------------------------------------------------------------------------------------------------
    GetLog() << "Load driver... \n";
    // Create both a GUI driver and a path-follower and allow switching between them
    ChIrrGuiDriver driver_gui(app);

    GetLog() << "Getting path... \n";
    // From data file
    // auto path = ChBezierCurve::read(vehicle::GetDataFile(path_file));
    auto path = ChBezierCurve::read("E:/doba/path_following_in_chrono/path/test_200x200/path_Euc.txt");
    // auto path = ChBezierCurve::read("E:/doba/path_following_in_chrono/path/test_200x200/path_ZHeur.txt");
    // auto path = ChBezierCurve::read("E:/doba/path_following_in_chrono/path/test_200x200/path_GradHeur.txt");
    // auto path = ChBezierCurve::read("E:/doba/path_following_in_chrono/path/test_200x200/path_RolloverHeur.txt");

#ifdef USE_PID
    ChPathFollowerDriver driver_follower(my_hmmwv.GetVehicle(), path, "my_path", target_speed);
    driver_follower.GetSteeringController().SetLookAheadDistance(8);
    driver_follower.GetSteeringController().SetGains(0.5, 0, 0);
    driver_follower.GetSpeedController().SetGains(0.7, 0.00, 0);
    driver_follower.Initialize();

    // Create and register a custom Irrlicht event receiver to allow selecting the
    // current driver model.
    MY_DRIVER::ChDriverSelector selector(my_hmmwv.GetVehicle(), &driver_follower, &driver_gui);
#endif
#ifdef USE_XT
    // ChPathFollowerDriverXT driver_follower(my_hmmwv.GetVehicle(), path, "my_path", target_speed,
    // my_hmmwv.GetVehicle().GetMaxSteeringAngle());
    ChPathFollowerDriverXT driver_follower(my_hmmwv.GetVehicle(), path, "my_path", target_speed, my_hmmwv.GetVehicle().GetMaxSteeringAngle());
    driver_follower.GetSteeringController().SetLookAheadDistance(5);
    driver_follower.GetSteeringController().SetGains(0.4, 1, 1, 1);
    driver_follower.GetSpeedController().SetGains(0.4, 0, 0);
    driver_follower.Initialize();

    // Create and register a custom Irrlicht event receiver to allow selecting the
    // current driver model.
    MY_DRIVER::ChDriverSelector selector(my_hmmwv.GetVehicle(), &driver_follower, &driver_gui);
#endif
#ifdef USE_ST
    const double axle_space = 3.2;
    const bool path_is_closed = false;
    ChPathFollowerDriverStanley driver_follower(my_hmmwv.GetVehicle(), path, "my_path", target_speed, path_is_closed,
                                           my_hmmwv.GetVehicle().GetMaxSteeringAngle());
    // driver_follower.GetSteeringController().SetLookAheadDistance(5);
    driver_follower.GetSteeringController().SetGains(1, 1, 0);
    driver_follower.GetSpeedController().SetGains(0.4, 0, 0);
    driver_follower.Initialize();

    // Create and register a custom Irrlicht event receiver to allow selecting the
    // current driver model.
    MY_DRIVER::ChDriverSelector selector(my_hmmwv.GetVehicle(), &driver_follower, &driver_gui);
#endif
    // Visualization of controller points (sentinel & target)
    irr::scene::IMeshSceneNode* ballS = app.GetSceneManager()->addSphereSceneNode(0.1f);
    irr::scene::IMeshSceneNode* ballT = app.GetSceneManager()->addSphereSceneNode(0.1f);
    ballS->getMaterial(0).EmissiveColor = irr::video::SColor(0, 255, 0, 0);
    ballT->getMaterial(0).EmissiveColor = irr::video::SColor(0, 0, 255, 0);

    // Allow the keyboard events
    app.SetUserEventReceiver(&selector);

    GetLog() << "Driver initialized.\n";



    // ---------------------------------------------------------------------------------------------------------------
 
/* //TODO: LOGGING
    // If in playback mode, attach the data file to the driver system and
    // force it to playback the driver inputs.
    if (driver_mode == PLAYBACK) {
        driver.SetInputDataFile(driver_file);
        driver.SetInputMode(ChIrrGuiDriver::DATAFILE);
    }
*/
    // ---------------------------------------------------------------------------------------------------------------
    GetLog() << "GUI setting...\n";
    app.SetHUDLocation(500, 20);
    app.SetSkyBox();
    app.AddTypicalLights(irr::core::vector3df(130.f, -130.f, 100.f), irr::core::vector3df(130.f, 130.f, 100.f), 500, 500);
    app.SetChaseCamera(trackPoint, 6.0, 0.5);
    app.SetTimestep(step_size);
    app.AssetBindAll(); //this will visualize the path if it is added earlier
    app.AssetUpdateAll();

    // ---------------
    // Simulation loop
    // ---------------

    my_hmmwv.GetVehicle().LogSubsystemTypes();

    if (debug_output) {
        GetLog() << "\n\n============ System Configuration ============\n";
        // my_hmmwv.LogHardpointLocations();
    }

    // Number of simulation steps between miscellaneous events
    int render_steps = (int)std::ceil(render_step_size / step_size);
    int debug_steps = (int)std::ceil(debug_step_size / step_size);

    // Initialize simulation frame counters
    int step_number = 0;
    int render_frame = 0;

    if (contact_vis) {
        app.SetSymbolscale(1e-4);
        app.SetContactsDrawMode(ChIrrTools::eCh_ContactsDrawMode::CONTACT_FORCES);
    }

    ChRealtimeStepTimer realtime_timer;
 
    while (app.GetDevice()->run()) {
        double time = system->GetChTime();

        // End simulation
        if (time >= t_end)
            break;

        // Render scene and output POV-Ray data
        if (step_number % render_steps == 0) {
            app.BeginScene(true, true, irr::video::SColor(255, 140, 161, 192));
            app.DrawAll();
            app.EndScene();

            render_frame++;
/* //TODO: LOGGING
            if (povray_output) {
                char filename[100];
                sprintf(filename, "%s/data_%03d.dat", pov_dir.c_str(), render_frame);
                utils::WriteShapesPovray(my_hmmwv.GetSystem(), filename);
            }
*/
            
        }



        // Driver inputs
        ChDriver::Inputs driver_inputs = selector.GetDriver()->GetInputs();
        // Update sentinel and target location markers for the path-follower controller.
        // Note that we do this whether or not we are currently using the path-follower driver.
        const ChVector<>& pS = driver_follower.GetSteeringController().GetSentinelLocation();
        const ChVector<>& pT = driver_follower.GetSteeringController().GetTargetLocation();
        ballS->setPosition(irr::core::vector3df((irr::f32)pS.x(), (irr::f32)pS.y(), (irr::f32)pS.z()));
        ballT->setPosition(irr::core::vector3df((irr::f32)pT.x(), (irr::f32)pT.y(), (irr::f32)pS.z())); //Modification to keep it in the right altitude
        


        // Debug logging
        if (debug_output && step_number % debug_steps == 0) {
            GetLog() << "\n\n============ System Information ============\n";
            GetLog() << "Time = " << time << "\n\n";

            auto marker_driver = my_hmmwv.GetChassis()->GetMarkers()[0]->GetAbsCoord().pos;
            auto marker_com = my_hmmwv.GetChassis()->GetMarkers()[1]->GetAbsCoord().pos;
            GetLog() << "Markers\n";
            GetLog() << "  Driver loc:      " << marker_driver.x() << " " << marker_driver.y() << " "
                      << marker_driver.z() << "\n";
            GetLog() << "  Chassis COM loc: " << marker_com.x() << " " << marker_com.y() << " " << marker_com.z()
                      << "\n";

            GetLog() << "  Following point: " << pT.x() << " " << pT.y() << " " << pT.z() << "\n";
        }


        // Update modules (process inputs from other modules)
        driver_follower.Synchronize(time);
        driver_gui.Synchronize(time);
        terrain.Synchronize(time);
        my_hmmwv.Synchronize(time, driver_inputs, terrain);
        std::string msg = selector.UsingGUI() ? "GUI driver" : "Follower driver";
        app.Synchronize(msg, driver_inputs);

        // Advance simulation for one timestep for all modules
        driver_follower.Advance(step_size);
        driver_gui.Advance(step_size);
        terrain.Advance(step_size);
        my_hmmwv.Advance(step_size);
        system->DoStepDynamics(step_size);
        app.Advance(step_size);

        // Increment frame number
        step_number++;

        // Spin in place for real time to catch up
        realtime_timer.Spin(step_size);


        // Driver output
        if (driver_mode == RECORD) {
            chrono::Vector global_pos = my_hmmwv.GetVehicle().GetVehiclePos();
            chrono::Vector global_rot = my_hmmwv.GetVehicle().GetVehicleRot().Q_to_Euler123();
            double global_vel = my_hmmwv.GetVehicle().GetVehicleSpeed();

            driver_csv << time << global_pos.x() << global_pos.y() << global_pos.z() << global_rot.x() << global_rot.y()  << global_rot.z() << global_vel << driver_inputs.m_steering << driver_inputs.m_throttle << driver_inputs.m_braking << std::endl;
        }

    }

    if (driver_mode == RECORD) {
        std::string csv_header = "Time PosX PosY PosZ RotX RotY RotZ Velocity Steering Throttle Braking\n";
        driver_csv.write_to_file(driver_file, csv_header);
    }

    return 0;
}
