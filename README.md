# Path_following_in_chrono
General framework for testing path planner and following algorithms in Chrono simulation.


## Project Chrono
https://projectchrono.org/
Simulator equiped with 3D, defomrable terrain and vehicle simulation.

## TODO list
- Adding Deformable Soil calibration based on a soil map
- Adding constant velocity control with keyboard
- Adding path following:
    a) read from file
    b) create interface for ROS?
- Adding logging system

